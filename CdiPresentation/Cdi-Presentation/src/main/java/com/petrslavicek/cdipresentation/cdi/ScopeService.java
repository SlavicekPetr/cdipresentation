package com.petrslavicek.cdipresentation.cdi;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

/**
 *
 * @author slavicekp
 */


public class ScopeService implements Serializable {
    
    Integer counter = 0;
    

   
    @PostConstruct
    public void postConstruct () {
        String test = "";
       
    }
    
    @PreDestroy
    public void preDestroy () {
        String test = "";        
    }
    
    
    public void addCounter() {
        counter++;
    }
    
    public String getCounterString() {
        return String.valueOf(counter);
    }

}
