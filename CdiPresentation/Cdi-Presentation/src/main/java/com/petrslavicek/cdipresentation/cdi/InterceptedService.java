package com.petrslavicek.cdipresentation.cdi;

import java.util.logging.Logger;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author slavicekp
 */
public class InterceptedService {

    @Inject
    private Logger logger;

    public String getInterceptedText() {
        return "Intercepted text";
    }

    @AroundInvoke
    private Object logMethodCall(InvocationContext ic) throws Exception {
        logger.entering(ic.getTarget().toString(), ic.getMethod().getName());
        try {
            return ic.proceed();
        } finally {
            logger.exiting(ic.getTarget().toString(), ic.getMethod().getName());
        }
    }

}
