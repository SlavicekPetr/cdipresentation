package com.petrslavicek.cdipresentation.cdi;

/**
 *
 * @author slavicekp
 */
public interface IRefNumberGenerator {
    
    String getRefNumber();

}
