package com.petrslavicek.cdipresentation.cdi;

import javax.enterprise.inject.Produces;

/**
 *
 * @author slavicekp
 */
public class RefNumberPrefix {
    
    @Produces @com.petrslavicek.cdipresentation.annotation.RefNumberPrefix
    public String getRefNumberPrefix() {
        return "PFX ";
    }

}
