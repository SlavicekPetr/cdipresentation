package com.petrslavicek.cdipresentation.cdi;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author slavicekp
 */
@Named
public class RefNumberGenerator implements IRefNumberGenerator, Serializable {

    @Inject
    ScopeService scopeService;
    
    @Inject
    ScopeService scopeService2;
    
    @Inject
    ConversationService conversationService;
    
    public RefNumberGenerator() {
        //this.scopeService = scopeService;
        
    }

    @Override
    public String getRefNumber() {
        
        this.scopeService.addCounter();
        this.scopeService.getCounterString();

        this.scopeService2.addCounter();
        this.scopeService2.getCounterString();
        return "123/TST";
    }
    
    public void startConversation() {
        conversationService.start();
    }
    
    public void proceedConversation() {
        conversationService.proceed();
    }
    
    public void endConversation() {
        conversationService.end();
    }

    public String getPrefix() {
        return "ČJ ";
    }
    
    @PostConstruct
    public void init() {
        
    }

}
