package com.petrslavicek.cdipresentation.cdi;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author slavicekp
 */
@Named
@ConversationScoped
public class ConversationService implements Serializable {
    
        
    @Inject
    private Conversation conversation;
    
    
    static final Logger logger = Logger.getLogger("conversation");
    
    @PostConstruct
    public void init() {
        logger.log(Level.INFO, "ConversationService construncted - " + this.toString());
    }
    
    public void start() {
        conversation.begin();
        logger.log(Level.INFO, "Conversation starts - number " + conversation.getId());
    }
    
    public void proceed() {
        logger.log(Level.INFO, "Conversation proceeds  - number " + conversation.getId());
    }
    
    public void end() {        
        logger.log(Level.INFO, "Conversation ends - number " + conversation.getId());
        conversation.end();
    }
    
    @PreDestroy
    public void cleanUp() {
        logger.log(Level.INFO, "ConversationService destructed - " + this.toString());
    }
    
    public String getConversationId() {
        return conversation.getId();
    }
    
    public boolean isTransient() {
        return conversation.isTransient();
    }
    
    public String getLinkToSecondStep() {
        return "conversation_proceed.jsf?cid=" + getConversationId();
    }
        
    public String handleConverstionEnd() {
        end();
        return "conversation_start.jsf?faces-redirect=true";
    }

}
