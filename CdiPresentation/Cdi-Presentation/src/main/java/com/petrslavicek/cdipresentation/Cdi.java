package com.petrslavicek.cdipresentation;

import com.petrslavicek.cdipresentation.annotation.RefNumberPrefix;
import com.petrslavicek.cdipresentation.cdi.ConversationService;
import com.petrslavicek.cdipresentation.cdi.IRefNumberGenerator;
import com.petrslavicek.cdipresentation.cdi.ScopeService;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 *
 * @author slavicekp
 */
public @Model
class Cdi {

    @Inject
    private IRefNumberGenerator refNumberGenerator;

    @Inject
    ConversationService conversationService;

    @Inject
    @RefNumberPrefix
    private String prefix;

    public Cdi() {

    }

    public String getRefNumber() {
        return refNumberGenerator.getRefNumber();
    }

    public String getPrefix() {
        return prefix;
    }

    public void startConversation() {
        conversationService.start();
    }

    public void proceedConversation() {
        conversationService.proceed();
    }

    public void endConversation() {
        conversationService.end();
    }
    
    public String getConversationId() {
        return conversationService.getConversationId();
    }

}
