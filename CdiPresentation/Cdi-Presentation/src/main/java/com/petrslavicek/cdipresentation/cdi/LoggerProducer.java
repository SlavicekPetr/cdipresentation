package com.petrslavicek.cdipresentation.cdi;

import java.util.logging.Logger;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * Trida vytvari Logger s nastavenim parametru - nazvu tridy do ktere bude injectovan
 * @author slavicekp
 */
public class LoggerProducer {
    
    @Produces
    public Logger createLogger(InjectionPoint ip) {
        return Logger.getLogger(ip.getMember().getDeclaringClass().getName());
    }

}
